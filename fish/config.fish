if status is-interactive
    and not set -q TMUX
    exec tmux
end

set fish_greeting ""

set -gx TERM xterm-256color

# Binds
bind -k nul forward-char
#accept-autosuggestion

# aliases
alias ll "ls -lh"
alias k kubectl
alias vim nvim
alias qc "git add -A && git commit -m '💫 WiP'"
alias gs "git status"

set -gx EDITOR nvim
set -gx SHELL /bin/fish

set -gx PATH /usr/lib/lightdm/lightdm:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games $PATH

# Nvim
set -g NVIM /opt/nvim-linux64/bin
set -gx PATH $NVIM $PATH

# Go
set -g GOPATH /usr/local/go/bin
set -gx PATH $GOPATH $PATH
