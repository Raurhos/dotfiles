# dotfiles

## Contents

- vim ([Neovim](https://github.com/neovim/neovim/blob/master/INSTALL.md)) config
- [Fish](https://fishshell.com/) config


## Neovim setup

- [Neovim](https://github.com/neovim/neovim/blob/master/INSTALL.md) >= 0.9.0
- [Git](https://www.git-scm.com/)
- [LazyVim](https://github.com/LazyVim/LazyVim)
- a [Nerd Font](https://www.nerdfonts.com/)
- a C compiler (gcc, etc...)
- [Solarized-osaka](https://github.com/craftzdog/solarized-osaka.nvim) - theme


## Fish setup

- [Fish](https://fishshell.com/) shell
- [Fisher](https://github.com/jorgebucaran/fisher) - Plugin manager
- [Tide](https://github.com/IlanCosman/tide) - Shell theme
- [Nerd Font](https://www.nerdfonts.com/)
