return {
  {
    "nvim-treesitter/nvim-treesitter",
    opts = {
      ensure_installed = {
        "cmake",
        "css",
        "fish",
        "gitignore",
        "go",
        "graphql",
        "http",
        "scss",
        "sql",
        "bash",
        "html",
        "json",
        "markdown",
        "yaml",
        "templ",
      },

      -- matchup = {
      -- 	enable = true,
      -- },
    },
    config = function(_, opts)
      require("nvim-treesitter.configs").setup(opts)

      -- MDX
      vim.filetype.add({
        extension = {
          mdx = "mdx",
          templ = "templ",
        },
      })
      vim.treesitter.language.register("markdown", "mdx")
    end,
  },
}
